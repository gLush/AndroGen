import shutil
import pathlib
from rich import print
import configparser
import validators

from pathlib import Path

import logging
from rich.logging import RichHandler

from string import Template

import argparse

import git

parser = argparse.ArgumentParser()

#python3 androgen.py --project AndrogenTest --domain com.a2raco --git https://gitlab.com/gLush/androgentemplate

parser.add_argument("--project", "-p", help="set project name")
parser.add_argument("--domain", "-d", help="set project domain name")
parser.add_argument("--git", "-g", help="set template git repository")

# Read arguments from the command line
args = parser.parse_args()

def setuplogger():
    FORMAT = "%(message)s"
    logging.basicConfig(level="NOTSET", format=FORMAT, datefmt="[%X]", handlers=[RichHandler()])
    return logging.getLogger("rich")

log = setuplogger()

def readagconfig(filename):
    if not filename.exists():
        log.error("---> Config file androgen.ini does not exist.")
        quit(404)
    else:
        log.info("---> Loading config...")
    configparserobject = configparser.ConfigParser()
    configparserobject.read(filename)
    return configparserobject

def isvaliddomainprefix(prefix):
    if validators.domain(prefix) and len(prefix.split(".")) == 2:
        return True
    else:
        return False

scriptpath = pathlib.Path(__file__).resolve().parent

configfilename = scriptpath / "androgen.ini"
localtemplatepath = scriptpath / "template"
localprojectpath = scriptpath / "project"

config = readagconfig(configfilename)

exts = config.get("DEFAULTS", "exts").strip().split('|')

if args.domain:
    domain = args.domain.strip()
else:
    domain = config.get("DEFAULTS", "domainprefix").strip()

if not isvaliddomainprefix(domain):
    log.error(f'---> Domain prefix {domain} is not valid')
    quit(500)

if args.project:
    projectname = args.project.strip()
else:
    projectname = config.get("DEFAULTS", "projectname")

(domain1, domain2) = domain.split(".")

packagename = f'{domain1}.{domain2}.{projectname.lower()}'

substs = {k.upper():v for k, v in config['SUBSTS'].items()}

substs["PROJECTNAME"] = projectname
substs["PACKAGENAME"] = packagename
substs["DOMAIN1"] = domain1
substs["DOMAIN2"] = domain2

log.info(f'---> Generate {projectname} as package {packagename}...')

if not localtemplatepath.exists():
    log.error("---> Local template directory does not exist.")
    quit(404)

if localprojectpath.exists():
    log.debug("---> Clean existing project directory...")
    shutil.rmtree(localprojectpath, ignore_errors=True, onerror=None)     

if args.git:
    gitpath = args.git.strip()
    Path(localprojectpath).mkdir(parents=True, exist_ok=True)
#    git.Git(localprojectpath).clone("https://gitlab.com/gLush/androgentemplate.git")
    git.Repo.clone_from("https://gitlab.com/gLush/androgentemplate.git", localprojectpath)

else:
    log.info("---> Use local template dir...")
    projectname = config.get("DEFAULTS", "projectname")
    log.debug(f'---> Copy project template to destination directory {localprojectpath}')
    shutil.copytree(localtemplatepath, localprojectpath, dirs_exist_ok=True) 

log.debug(f'---> Rename project directory to {projectname.lower()}')

activityfilename = projectname + "Activity.kt"
applicationfilename = projectname + "Application.kt"

shutil.move(
    scriptpath / "project" / "app" / "src" / "main" / "kotlin" / "domain1" / "domain2" / "project" / "MainActivity.kt",
    scriptpath / "project" / "app" / "src" / "main" / "kotlin" / "domain1" / "domain2" / "project" / activityfilename
)

shutil.move(
    scriptpath / "project" / "app" / "src" / "main" / "kotlin" / "domain1" / "domain2" / "project" / "MainApplication.kt",
    scriptpath / "project" / "app" / "src" / "main" / "kotlin" / "domain1" / "domain2" / "project" / applicationfilename
)

shutil.move(
    scriptpath / "project" / "app" / "src" / "main" / "kotlin" / "domain1" / "domain2" / "project",
    scriptpath / "project" / "app" / "src" / "main" / "kotlin" / "domain1" / "domain2" / f'{projectname.lower()}'
)

log.debug(f'---> Rename domains to {domain1.lower()}.{domain2.lower()}')

shutil.move(
    scriptpath / "project" / "app" / "src" / "main" / "kotlin" / "domain1" / "domain2",
    scriptpath / "project" / "app" / "src" / "main" / "kotlin" / "domain1" / f'{domain2.lower()}'
)

shutil.move(
    scriptpath / "project" / "app" / "src" / "main" / "kotlin" / "domain1",
    scriptpath / "project" / "app" / "src" / "main" / "kotlin" / f'{domain1.lower()}'
)

filelist = []

for ext in exts:
    filelist += [str(item) for item in Path(localprojectpath).glob("**/*." + ext) if Path(item).is_file()]

def processfilewithsubsts(filename, substs):
    with open(filename, 'r') as f:
        src = Template(f.read())
        result = src.safe_substitute(substs)
        f.close()
    with open(filename, 'w') as f:
        f.write(result)
        f.close()
    return

for everyfile in filelist:
    log.debug(f'---> Process file {everyfile}')
    processfilewithsubsts(everyfile, substs)