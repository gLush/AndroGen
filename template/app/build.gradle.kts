plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("plugin.serialization") version "1.6.10"
    id("de.nanogiants.android-versioning") version "2.4.0"
}

android {
    compileSdk = $COMPILESDK

    defaultConfig {
        applicationId = "$PACKAGENAME"
        minSdk = $MINSDK
        targetSdk = $TARGETSDK
        versionCode = versioning.getVersionCode()
        versionName = versioning.getVersionName()
        setProperty("archivesBaseName", "$PROJECTNAME")
    }
    buildTypes {
        getByName("debug") {
            isMinifyEnabled = false
            applicationIdSuffix = ".debug"
        }
        getByName("release"){
            isMinifyEnabled = true
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    kotlinOptions {
        freeCompilerArgs = freeCompilerArgs + "-Xopt-in=kotlin.RequiresOptIn" + "-Xinline-classes"
    }
    buildFeatures {
        viewBinding = true
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.1.0-rc01"
    }
    packagingOptions {
        resources {
            excludes += mutableSetOf("/META-INF/{AL2.0,LGPL2.1}")
            excludes += mutableSetOf("/META-INF/DEPENDENCIES")
        }
    }
    sourceSets {
        // Get an AndroidSourceSet by name
        named("main") {
            java.srcDirs("src/main/kotlin")
        }
    }
    lint {
        lintConfig=file("lint.xml")
    }
}

dependencies {
    implementation ("androidx.core:core-ktx:1.7.0")

    implementation ("androidx.appcompat:appcompat:1.4.1")
    implementation ("com.google.android.material:material:1.5.0")

    val composeVersion = "1.1.0-rc01"

    implementation ("androidx.compose.ui:ui:${composeVersion}")
    implementation ("androidx.compose.material:${composeVersion}")
    implementation ("androidx.compose.ui:ui-tooling:${composeVersion}")
    implementation ("androidx.compose.material:material-icons-extended:${composeVersion}")
    implementation ("androidx.compose.ui:ui-viewbinding:${composeVersion}")

    debugImplementation("androidx.compose.ui:ui-tooling:${composeVersion}")


//    implementation ("androidx.constraintlayout:constraintlayout-compose:1.0.0-beta02")

    implementation("androidx.lifecycle:lifecycle-runtime-ktx:2.4.0")
    implementation("androidx.lifecycle:lifecycle-viewmodel-compose:2.4.0")
    implementation("androidx.activity:activity-compose:1.4.0")

    // logging
    implementation ("com.jakewharton.timber:timber:5.0.1")

    debugImplementation ("com.squareup.leakcanary:leakcanary-android:2.7")

    // Serialization
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.3.2")

    // Koin
    implementation("io.insert-koin:koin-android:3.1.4")

}
